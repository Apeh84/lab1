#include <avr/pgmspace.h>

const char stud_name[] PROGMEM = "Gabriel test";

const char str_1[] PROGMEM = "January";
const char str_2[] PROGMEM = "February";
const char str_3[] PROGMEM = "March";
const char str_4[] PROGMEM = "April";
const char str_5[] PROGMEM = "May";
const char str_6[] PROGMEM = "June";

const char* const name_month[] PROGMEM = {
	str_1,
	str_2,
	str_3,
	str_4,
	str_5,
	str_6
};

#ifndef UART_H
#define UART_H

#include <stdio.h>
#include <avr/io.h>

extern FILE simple_uart0_io;
extern FILE simple_uart1_out;

void uart_init(void);

//FILE uart_io FDEV_SETUP_STREAM(RxTx_putchar, RxTx_getchar, _FDEV_SETUP_RW);

#endif /* UART_H */

#include <avr/io.h>
//#define __ASSERT_USE_STDERR
#include <assert.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <util/delay.h>
//#include "uart.h"
#include <avr/version.h>
#include <avr/pgmspace.h>
#include <ctype.h>
#include "hmi.h"
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr_uart/uart.h"

// Create microrl object and pointer to it
microrl_t rl;
microrl_t *prl = &rl;


#define BLINK_DELAY_MS 200
#define LED_RED         PORTA0 // Arduino Mega digital pin 22
#define LED_GREEN       PORTA2 // Arduino Mega digital pin 24
#define LED_BLUE        PORTA4 // Arduino Mega digital pin 26


static inline void init_leds(void)
{
    DDRA |= _BV(DDA0); // pin 22
    DDRA |= _BV(DDA2); // pin 24
    DDRA |= _BV(DDA4); // pin 26
    DDRB |= _BV(DDB7);
    PORTB &= ~_BV(PORTB7);
}


/* Init error console as stderr in UART1 and print user code info */
static inline void init_errcon(void)
{
    //uart_init();
	uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
	uart1_puts_p(PSTR("Version: "FW_VERSION, "built on: "__DATE__ " " __TIME__"\r\n"));
	uart1_puts_p(PSTR("avr-libc version: "__AVR_LIBC_VERSION_STRING__ " " "avr-gcc version: "__VERSION__ "\r\n"));
    
	/*
    stderr = &simple_uart1_out;
        fprintf(stderr,
                "Version: %s built on: %s %s\navr-libc version: %s avr-gcc version: %s\n",
                FW_VERSION, __DATE__, __TIME__,
                __AVR_LIBC_VERSION_STRING__, __VERSION__);*/
    
}

static void led_on(int led)
{
    PORTA |= _BV(led);
}
static void led_off(int led)
{
    PORTA &= ~_BV(led);
}

/*
static inline void blink_leds(void)
{
    led_on(LED_BLUE);
    _delay_ms(BLINK_DELAY_MS);
    led_off(LED_BLUE);
    led_on(LED_GREEN);
    _delay_ms(BLINK_DELAY_MS);
    led_off(LED_GREEN);
    led_on(LED_RED);
    _delay_ms(BLINK_DELAY_MS);
    led_off(LED_RED);
}*/


static inline void init_ioconn(void) {
	uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
	uart0_puts_p(PSTR("console started\r\n"));
	uart0_puts_p(stud_name);
	uart0_puts_p(PSTR("\r\n"));
}
static inline void init_lcd(void) {
	lcd_init();
	lcd_goto(LCD_ROW_2_START);
	lcd_puts_P(stud_name);
	
	// Call init with ptr to microrl instance and print callback
	microrl_init(prl, uart0_puts);
	// Set callback for execute
	microrl_set_execute_callback(prl, cli_execute);
}
static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}
static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        //Print uptime to uart1
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        //Toggle LED
        PORTA ^= _BV(LED_GREEN);
        prev_time = now;
    }
}
  /*
    uart_init();
        stdin = stdout = &simple_uart0_io;
        fprintf_P(stdout, stud_name);
        fprintf(stdout, "\n");
        fprintf_P(stdout, PSTR("Console started\n"));*/
    


void print_ascii_tbl(void) {
    for (char c = ' '; c <= '~'; c++) {
		uart0_putc(c);
    }

    uart0_puts_p(PSTR("\r\n"));
}

void print_for_human (const unsigned char *array, const size_t len) {
	for (size_t i = 0; i < len; i++) {
	        if (array[i] >= ' ' && array[i] <= '~') {
	            uart0_putc(array[i]);
	        } else {
	            uart0_puts_p(PSTR("\"0x"));
	            uart0_putc((array[i] >> 4) + ((array[i] >> 4) <= 9 ? 0x30 : 0x37));
	            uart0_putc((array[i] & 0x0F) + ((array[i] & 0x0F) <= 9 ? 0x30 : 0x37));
	            uart0_putc('"');
        }
    }

    uart0_puts_p(PSTR("\r\n"));
}

void main(void)
{
    uart_init();
    init_leds();
    init_errcon();
	init_ioconn();
	int_sys_timer();
	sei();
    init_lcd();
    lcd_clrscr();
	lab4_month_lookup();
    print_ascii_tbl();
	
	while (1) {
	    heartbeat();
	    // CLI commands are handled in cli_execute()
	    microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
	}
    uint8_t array[128] = {0};

    for (uint8_t i = 0; i < 128; i++) {
        array[i] = i;
    }
	

    print_for_human(array, 128);
    lcd_puts_P(stud_name);
}


static inline void lab4_month_lookup(void)
{
	static char in_buf = -1
		if (in_buf == -1){
			uart0_puts_p(PSTR("Enter Month name first letter >"));
		}

    

    /*
    while (1) {
            blink_leds();
            uart0_puts_p(PSTR("Enter Month name first letter >"));
            str = toupper(getchar());
            fprintf(stdout, "%c\n", str);
            
    		array = realloc( array, (i++ * 100) * sizeof(char));
            fprintf(stderr, "Free space: %d\n",
                            (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval));
                    assert(array != NULL);*/
    
        uart0_putc((uint8_t) in_buf);
		uart0_puts_p(PSTR("\r\n"));
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        lcd_goto(LCD_ROW_2_START);

        for (uint8_t i = 0; i < 6; i++) {
            if (!strncmp_P(&str, (PGM_P)pgm_read_word(&(name_month[i])), 1)) {
                uart0_put_P(stdout, (PGM_P)pgm_read_word(&(name_month[i])));
                uart0_put_P(stdout, "\n");
                lcd_puts_P((PGM_P)pgm_read_word(&(name_month[i])));
                lcd_puts(" ");
			}
		}
		//in_buf = -1;
ISR(TIMER1_COMPA_vect)
}

	system_tick()
}


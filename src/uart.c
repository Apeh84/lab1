#include <avr/io.h>
#include <stdio.h>

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#ifndef BAUD
#define BAUD 9600
#endif
#include <util/setbaud.h>

int put_char(char c, FILE *stream);
int get_char(FILE *stream);
int put_char_stderr(char c, FILE *stream);

FILE simple_uart0_io = FDEV_SETUP_STREAM(put_char, get_char, _FDEV_SETUP_RW);
//FILE uart_input = FDEV_SETUP_STREAM(NULL, get_char, _FDEV_SETUP_READ);
FILE simple_uart1_out = FDEV_SETUP_STREAM(put_char_stderr, NULL, _FDEV_SETUP_WRITE);

void uart_init(void)
{
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
    UBRR1H = UBRRH_VALUE;
    UBRR1L = UBRRL_VALUE;
#if USE_2X
    UCSR0A |= _BV(U2X0);
    UCSR1A |= _BV(U2X1);
#else
    UCSR0A &= ~(_BV(U2X0));
    UCSR1A &= ~(_BV(U2X1));
#endif
    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
    UCSR1C = _BV(UCSZ11) | _BV(UCSZ10); /* 8-bit data */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0);   /* Enable RX and TX */
    UCSR1B = _BV(TXEN1); /* Enable TX */
}

int put_char(char c, FILE *stream)
{
    if (c == '\n') {
        put_char('\r', stream);
    }

    loop_until_bit_is_set(UCSR0A, UDRE0);
    UDR0 = c;
    return 1;
}

int put_char_stderr(char c, FILE *stream)
{
    if (c == '\n') {
        put_char_stderr('\r', stream);
    }

    loop_until_bit_is_set(UCSR1A, UDRE1);
    UDR1 = c;
    return UDR1;
}

int get_char(FILE *stream)
{
    (void) stream;
    loop_until_bit_is_set(UCSR0A, RXC0);
    return UDR0;
}
